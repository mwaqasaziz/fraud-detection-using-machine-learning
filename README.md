# Data Mining Project
## Tasks Overview
The tasks for the project include

- Determining a dataset that is both significant and valuable enough to add value to the real world problem

- A look at the business questions that the project is trying to answer
  * The background of fraud transaction and why it is becoming problematic
  * The description of our dataset and how our project help understand the fraud transactions

- Preparing the dataset, including:
  * Introduction to the credit card transaction fraud dataset
  * Check the missing data 
  * Check the repetitive data
  * Cleanse the missing data
  
- A thorough EDA for the dataset, including:
  * Transform the target variable from continuous variable to categorical variable (0 and 1 to 'Not Fraud' and 'Fraud')
  * Transform the transaction time variable from number of seconds elapsed to hours in a day, then converted the hours into day and night
  * Look into multivariable correlation
  * Analyze original variables
  * Analyze masked variables, from a macro level to a micro level
  * Recommend the variables that could be useful for predicting the fraud
  
- Adapting machine learning modeling for the dataset, including:
  * A Random Forest model
  * A Gradiant Boosting model
  * A K-Nearest Neighbor model
  * Model assessment individually, including ROC curve, confusion matrix, and evaluation of variable importance
  * Model comparison on sensitivity rate



